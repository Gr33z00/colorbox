﻿using System;
using System.ComponentModel;
using PluginCore.Localization;

namespace ColorBox
{
    public enum DisplayPosition
    {
        AboveColorValue = -1,
        BelowColorValue = 1
    }

    [Serializable]
    public class Settings
    {
        const DisplayPosition DEFAULT_DISPLAY_POSITION = DisplayPosition.BelowColorValue;

        protected DisplayPosition tooltipDisplayPosition = DEFAULT_DISPLAY_POSITION;

        [DisplayName("Tooltip display location")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("Determines, where the tooltip shall be displayed"), DefaultValue(DEFAULT_DISPLAY_POSITION)]
        public DisplayPosition TooltipDisplayPosition
        {
            get { return tooltipDisplayPosition; }
            set { tooltipDisplayPosition = value; }
        }

        const int DELAY = 750;

        protected int delay = DELAY;

        [DisplayName("Tooltip display delay")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("Determines how long the mouse has to stay on a color-value before the tooltip is displayed"), DefaultValue(DELAY)]
        public int Delay
        {
            get { return delay; }
            set { delay = value; }
        }

        public static string[] COLOR_TRIGGERS = new string[] {
            ///"0x%2hR%%2hG%%2hB%","#%2hR%%2hG%%2hB%","#%1hR%%1hG%%1hB%","rgb(%3dR%,%3dG%,%3dB%)"
            @"\#(?<Rh>[0-9a-fA-F]?)(?<Gh>[0-9a-fA-F]?)(?<Bh>[0-9a-fA-F])\b",            
            @"\#(?<Rh>[0-9a-fA-F]{0,2})(?<Gh>[0-9a-fA-F]{0,2})(?<Bh>[0-9a-fA-F]{1,2})\b",
            @"0x(?<Rh>[0-9a-fA-F]{0,2})(?<Gh>[0-9a-fA-F]{0,2})(?<Bh>[0-9a-fA-F]{1,2})\b",
            @"rgb\((\s*(?<Rd>\d{1,3})\s*,\s*)?(\s*(?<Gd>\d{1,3})\s*,\s*)?(?<Bd>\d{1,3})\)?"
        };

        protected string[] triggers = COLOR_TRIGGERS;

        [DisplayName("Recognition Patterns")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("Determines the signs that indicate color-values that shall be parsed")]
        public string[] Patterns
        {
            get { return triggers; }
            set { triggers = value; }
        }

        const int TIP_WIDTH = 52;

        protected int tipWidth = TIP_WIDTH;

        [DisplayName("Tooltip-width")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("The width of the tooltip")]
        public int TipWidth
        {
            get { return tipWidth - 2; }
            set { tipWidth = value + 2; }
        }

        const int TIP_HEIGHT = 22;
        protected int tipHeight = TIP_HEIGHT;

        [DisplayName("Tooltip-height")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("The height of the tooltip")]
        public int TipHeight
        {
            get { return tipHeight - 2; }
            set { tipHeight = value + 2; }
        }

        const Boolean KEEP_ON_CURSOR = true;

        protected Boolean keepOnCursor = KEEP_ON_CURSOR;

        [DisplayName("Active visibility")]
        [LocalizedCategory("ASCompletion.Category.Common"), Description("Defines if the tooltip should stay visible, if it is displayed for the color under the cursor"), DefaultValue(KEEP_ON_CURSOR)]
        public Boolean KeepOnCursor
        {
            get { return keepOnCursor; }
            set { keepOnCursor = value; }
        }
    }
}