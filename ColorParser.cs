﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Drawing;
using System.Windows.Forms;

namespace ColorBox
{
    class ColorParser
    {
        private string[] patterns;
        private int parsingPattern;
        private string handle;
        
		public string[] Patterns
		{
			get
			{
				return patterns;
			}
			
			set
			{
				patterns = value;
			}
		}

        public string Handle
        {
            get
            {
                return handle;
            }

            set
            {
                handle = value;
                parsingPattern = FindPattern(handle);
            }
        }

        public int ParseOffset
        {
            get
            {
                if (handle != null && handle.Length > 0 && parsingPattern >= 0)
                {
                    String pattern = patterns[parsingPattern];
                    RegexOptions rexops = RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.RightToLeft;
                    Match m = Regex.Match(handle, pattern, rexops);
                    if (m != null)
                    {
                        return m.Index;
                    }
                }
                return 0;
            }
        }
		
		public Boolean WillTrigger()
        {
            return this.WillTrigger(handle);
        }

		public Boolean WillTrigger(string found)
		{
            return CheckForTrigger(found);
		}

        protected Boolean CheckForTrigger(string found)
        {
            if (found == null || found.Length < 1)
                return false;
            return FindPattern(found) >= 0;
        }

        protected int FindPattern(string found)
        {
            RegexOptions regxops = RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.ExplicitCapture | RegexOptions.RightToLeft;
            string pattern;

            for (int i = 0; i < patterns.Length; i++)
            {
                pattern = patterns[i];
                if (Regex.IsMatch(found, pattern, regxops))
                {
                    return i;
                }
            }
            return -1;
        }

        public Color ParseColor()
        {
            return this.ParseColor(handle);
        }

        public Color ParseColor(string found)
        {
            if (parsingPattern < 0)
                return new Color();

            string pattern = patterns[parsingPattern];

            int red;
            int green;
            int blue;
            red = green = blue = 0;

            //MessageBox.Show("Using pattern: '" + pattern + "' for found string: '"+found+"'");

            MatchCollection matches = Regex.Matches(found, pattern);
            Boolean simpleTriple;
            String value;
            foreach (Match match in matches)
            {
                simpleTriple = (match.Groups["Rh"].Value.Length == 1 && match.Groups["Gh"].Value.Length == 1 && match.Groups["Bh"].Value.Length == 1);

                if (match.Groups["Rh"].Value != "")
                {
                    value = match.Groups["Rh"].Value;
                    if (simpleTriple)
                    {
                        value = value + value;
                    }
                    red = int.Parse(value, NumberStyles.HexNumber);
                }
                else if (match.Groups["Rd"].Value != "")
                    red = int.Parse(match.Groups["Rd"].Value, NumberStyles.Integer);

                if (match.Groups["Gh"].Value != "")
                {
                    value = match.Groups["Gh"].Value;
                    if (simpleTriple)
                    {
                        value = value + value;
                    }
                    green = int.Parse(value, NumberStyles.HexNumber);
                }
                else if (match.Groups["Gd"].Value != "")
                    green = int.Parse(match.Groups["Gd"].Value, NumberStyles.Integer);

                if (match.Groups["Bh"].Value != "")
                {
                    value = match.Groups["Bh"].Value;
                    if (simpleTriple)
                    {
                        value = value + value;
                    }
                    blue = int.Parse(value, NumberStyles.HexNumber);
                }
                else if (match.Groups["Bd"].Value != "")
                    blue = int.Parse(match.Groups["Bd"].Value, NumberStyles.Integer);
            }
            if (red > 255)
                red = 255;
            else if (red < 0)
                red = 0;
            if (green > 255)
                green = 255;
            else if (green < 0)
                green = 0;
            if (blue > 255)
                blue = 255;
            else if (blue < 0)
                blue = 0;
            return Color.FromArgb(255, red, green, blue);
        }

        public string ParseWord(Color c)
        {
            RegexOptions rexops = RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.RightToLeft;
            string pattern = patterns[parsingPattern];

            List<string[]> groups = new List<string[]>();
            string[] infos;
            
            MatchCollection groupmatches = Regex.Matches(pattern, @"(?<color>[RGB])(?<type>[dh])[^{<]*({(\d+,)?(?<count>\d+))?", rexops);
            foreach (Match match in groupmatches)
            {
                infos = new string[3];
                infos[0] = match.Groups["color"].Value.ToUpper();
                infos[1] = match.Groups["type"].Value.ToLower();
                infos[2] = match.Groups["count"].Value;
                if (infos[2] == string.Empty)
                    infos[2] = "2";
                groups.Add(infos);
            }

            MatchCollection matches = Regex.Matches(handle, pattern, rexops);
            StringBuilder sb = new StringBuilder(handle);
            Group g;
            string v;
            string group;
            string type;
            byte cValue;
            foreach (Match match in matches)
            {
                for (int i = 0; i < groups.Count; i++)
                {
                    infos = groups[i];
                    group = infos[0] + infos[1];
                    g = match.Groups[group];
                    v = g.Value;
                    if (v != "")
                    {
                        cValue = GetColorPart(c, infos[0]);
                        type = (infos[1] == "h")?"X"+infos[2]:"G";
                        sb.Replace(g.Value, cValue.ToString(type), g.Index, g.Length);
                    }
                }
            }
            handle = sb.ToString();
            return handle;
        }

        protected byte GetColorPart(Color c, string part)
        {
            switch (part.ToLower())
            {
                case "r":
                    return c.R;
                case "g":
                    return c.G;
                case "b":
                    return c.B;
                case "a":
                    return c.A;
                default:
                    return new byte();
            }
        }
    }
}
