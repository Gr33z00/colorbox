using System;
using ColorBox.Controls;
using System.IO;
using PluginCore;
using PluginCore.Controls;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using PluginCore.Helpers;
using PluginCore.Utilities;
using ScintillaNet;

namespace ColorBox
{
    public class PluginMain : IPlugin, IEventHandler
    {
        #region Required fields
        private String pluginName = "ColorBox";
        private String pluginGuid = "9045A008-E98F-4153-AD97-858A17D19D87";
        private String pluginHelp = "http://flashtastic-fd-plugins.googlecode.com";
        private String pluginDesc = "This plugin lets you display the color you specified and change it";
        private String pluginAuth = "Griz (http://www.flashtastic.ch)";
        private String settingFilename;
        private Settings settingObject;
        private Image pluginImage;
        #endregion

        #region Custom fields
        private ScintillaControl currentSciControl;
        private ColorTip ctt;
        private ColorDialog colorDialog;
        private ColorParser cp;
        private SearchResult sr;
        private Point sciOffset;
        private Timer hideTimer;
        private bool isDialogOpen;
        private bool isListeningForExit;
        private bool isTipShown;
        private int lineHeight;
        private int lastPosition;
        #endregion

        #region Required Properties

        /// <summary>
        /// Name of the plugin
        /// </summary> 
        public String Name
		{
			get { return this.pluginName; }
		}

        /// <summary>
        /// Version of the API depending on the FD-version
        /// </summary>
        public Int32 Api
        {
            get { return 1; }
        }

        /// <summary>
        /// GUID of the plugin
        /// </summary>
        public String Guid
		{
			get { return this.pluginGuid; }
		}

        /// <summary>
        /// Author of the plugin
        /// </summary> 
        public String Author
		{
			get { return this.pluginAuth; }
		}

        /// <summary>
        /// Description of the plugin
        /// </summary> 
        public String Description
		{
			get { return this.pluginDesc; }
		}

        /// <summary>
        /// Web address for help
        /// </summary> 
        public String Help
		{
			get { return this.pluginHelp; }
		}

        /// <summary>
        /// Object that contains the settings
        /// </summary>
        [Browsable(false)]
        public Object Settings
        {
            get { return this.settingObject; }
        }
		
		#endregion
		
		#region Required Methods
		
		public void Initialize()
		{
            this.InitBasics();
            this.LoadSettings();
            this.AddEventHandlers();
            this.CreateToolTip();
        }

        public void InitBasics()
        {
            String dataPath = Path.Combine(PathHelper.DataDir, "ColorBox");
            if (!Directory.Exists(dataPath)) Directory.CreateDirectory(dataPath);
            this.settingFilename = Path.Combine(dataPath, "Settings.fdb");
            this.pluginImage = PluginBase.MainForm.FindImage("100");
            this.cp = new ColorParser();
            isDialogOpen = false;
            isListeningForExit = false;
            isTipShown = false;
        }

        public void AddEventHandlers()
        {
            UITools.Manager.OnMouseHover += new UITools.MouseHoverHandler(OnMouseHover);
            UITools.Manager.OnMouseHoverEnd += new UITools.MouseHoverHandler(OnMouseOut);
            UITools.Manager.OnTextChanged += new UITools.TextChangedHandler(OnTextChange);

            PluginCore.Managers.EventManager.AddEventHandler(this, EventType.UIRefresh | EventType.FileSwitch | EventType.FileClose);
        }

        public void Dispose()
		{
            UITools.Manager.OnMouseHover -= new UITools.MouseHoverHandler(OnMouseHover);
            UITools.Manager.OnMouseHoverEnd -= new UITools.MouseHoverHandler(OnMouseOut);
            UITools.Manager.OnTextChanged -= new UITools.TextChangedHandler(OnTextChange);

            PluginCore.Managers.EventManager.RemoveEventHandler(this);
            StopTimer();
            this.SaveSettings();
		}
		
		#endregion

        #region Tooltip handling
       
        private void CreateToolTip()
        {
            this.ctt = new ColorTip(PluginBase.MainForm, new Size(settingObject.TipWidth, settingObject.TipHeight));
            ctt.MouseHover += new EventHandler(ctt_MouseHover);
            ctt.MouseLeave += new EventHandler(ctt_MouseLeave);
            ctt.MouseClick += new MouseEventHandler(ctt_MouseClick);

            colorDialog = new ColorDialog();
            colorDialog.AllowFullOpen = true; // Allows the user to select a custom color
            colorDialog.FullOpen = true; //Opens the panel that allows to select custom colors
            colorDialog.ShowHelp = true; // Allows the user to get help

            hideTimer = new Timer();
            hideTimer.Interval = this.settingObject.Delay;
            hideTimer.Tick += new EventHandler(hideTimer_Tick);
        }

        protected void ctt_MouseHover(object sender, EventArgs e)
        {
            StopTimer();
        }

        protected void ctt_MouseLeave(object sender, EventArgs e)
        {
            if (!isDialogOpen)
            {
                isListeningForExit = true;
            }
        }

        protected void ctt_MouseClick(object sender, EventArgs e)
        {
            // Sets the initial color select to the current text color,
            // so that if the user cancels out, the original color is restored.
            colorDialog.Color = ctt.CurrentColor;

            StopTimer();
            isDialogOpen = true;

            /// Open color selection dialog box
            ///TODO: offer better color picker?
            DialogResult dr = colorDialog.ShowDialog();
            if (dr == DialogResult.OK && colorDialog.Color != ctt.CurrentColor)
            {
                    ctt.CurrentColor = colorDialog.Color;
                    int cursorPos = currentSciControl.CurrentPos;
                    currentSciControl.SetSel(sr.WordStart, sr.WordEnd);
                    currentSciControl.ReplaceSel(cp.ParseWord(colorDialog.Color));
                    currentSciControl.CurrentPos = cursorPos;
                    currentSciControl.SetSel(cursorPos, cursorPos);
            }
            isDialogOpen = false;
            isListeningForExit = true;
        }

        protected void StartTimer()
        {
            if(!hideTimer.Enabled)
                hideTimer.Start();
        }

        protected void StopTimer()
        {
            if(hideTimer.Enabled)
                hideTimer.Stop();
        }

        protected void hideTimer_Tick(object sender, EventArgs e)
        {
            HideToolTip();
        }

        public void HideToolTip()
        {
            StopTimer();
            ctt.Hide();
            isTipShown = false;
        }

        #endregion

        #region Trigger event reaction (mouse hover and key press)

        public void HandleEvent(object sender, NotifyEvent e, HandlingPriority priority)
        {
            switch (e.Type)
            {
                case EventType.UIRefresh:
                    if (isListeningForExit)
                    {
                        if (lastPosition != currentSciControl.CurrentPos)
                        {
                            if (!SelectionHelper.IsSameWord(currentSciControl, lastPosition, currentSciControl.CurrentPos))
                            {
                                isListeningForExit = false;
                                StartTimer();
                            }
                            else
                            {
                                lastPosition = currentSciControl.CurrentPos;
                            }
                        }
                    }
                    return;
                case EventType.FileSwitch:
                    StopTimer();
                    HideToolTip();
                    isListeningForExit = false;
                    SetupSciHandling(PluginBase.MainForm.CurrentDocument.SciControl);
                    break;
                case EventType.FileClose:
                    StopTimer();
                    HideToolTip();
                    isListeningForExit = false;
                    break;
            }
        }

        private void OnTextChange(ScintillaControl sci, int position, int length, int linesAdded)
        {
            if (linesAdded != 0)
                HideToolTip();

            HandleColorInteraction(sci, position, true);
        }

        private void OnMouseHover(ScintillaControl sci, int position)
        {
            HandleColorInteraction(sci, position);
        }

        private void OnMouseOut(ScintillaControl sci, int position)
        {
            if (isTipShown)
            {
                if (!settingObject.KeepOnCursor || !SelectionHelper.IsSameWord(sci, sci.CurrentPos, lastPosition))
                {
                    StartTimer();
                }
                else
                {
                    isListeningForExit = true;
                }
            }
        }

        private void HandleColorInteraction(ScintillaControl sci, int position)
        {
            HandleColorInteraction(sci, position, false);
        }

        private void HandleColorInteraction(ScintillaControl sci, int position, Boolean switchListen)
        {
            if(SelectionHelper.IsMultilineSelection(sci, position) || SelectionHelper.IsDeclinedStyle(sci, position))
            {
                return;
            }

            sr = SelectionHelper.SimpleWordSearch(sci, position);

            cp.Handle = sr.Word;

            if (!cp.WillTrigger())
            {
                sr = SelectionHelper.ExtendedWordSearch(sci, sr);
                cp.Handle = sr.Word;
                if (!cp.WillTrigger())
                {
                    return;
                }
            }

            StopTimer();

            int correctedStart = sr.WordStart + cp.ParseOffset;

            Point p = new Point(sci.PointXFromPosition(correctedStart), sci.PointYFromPosition(correctedStart));
            p.Offset(sciOffset);

            if (settingObject.TooltipDisplayPosition == DisplayPosition.AboveColorValue)
                p.Y -= ctt.Size.Height;
            else
                p.Y += lineHeight;

            lastPosition = position;

            ctt.ShowAtLocation(p, cp.ParseColor());
            isTipShown = true;
            isListeningForExit = switchListen;
        }
        #endregion

        #region Window handling

        private void SetupSciHandling(ScintillaControl sci)
        {
            this.SetupSciHandling(sci, 0);
        }

        private void SetupSciHandling(ScintillaControl sci, int position)
        {
            if (currentSciControl != null)
            {
                currentSciControl.SizeChanged -= new EventHandler(sci_SizeChanged);
            }

            if (sci != null)
            {
                currentSciControl = sci;
                ComputeOffset(position);

                sci.SizeChanged += new EventHandler(sci_SizeChanged);
            }
        }

        private void ComputeOffset()
        {
            this.ComputeOffset(0);
        }

        private void ComputeOffset(int position)
        {
            if (currentSciControl == null)
                return;

            Control c = currentSciControl;
            sciOffset = new Point(c.Left, c.Top);
            while (c.Parent != PluginBase.MainForm)
            {
                c = c.Parent;
                sciOffset.X += c.Left;
                sciOffset.Y += c.Top;
            }

            int lineNum = currentSciControl.LineFromPosition(position);
            if (lineNum < currentSciControl.LineCount - 1)
            {
                int pos = currentSciControl.PositionFromLine(lineNum + 1);
                lineHeight = currentSciControl.PointYFromPosition(pos) - currentSciControl.PointYFromPosition(position);
            }
            else if (lineNum > 0)
            {
                int pos = currentSciControl.PositionFromLine(lineNum - 1);
                lineHeight = currentSciControl.PointYFromPosition(position) - currentSciControl.PointYFromPosition(pos);
            }
        }

        private void sci_SizeChanged(object sender, EventArgs e)
        {
            ComputeOffset();
        }

        #endregion

        #region settings handling

        /// <summary>
        /// Loads the plugin settings
        /// </summary>
        public void LoadSettings()
        {
            this.settingObject = new Settings();
            if (!File.Exists(this.settingFilename))
            {
                this.SaveSettings();
                this.LoadSettings();
            }
            else
            {
                Object obj = ObjectSerializer.Deserialize(this.settingFilename, this.settingObject);
                this.settingObject = (Settings)obj;
                
                if (ctt != null)
                    ctt.Size = new Size(settingObject.TipWidth, settingObject.TipHeight);
                if (cp != null && settingObject.Patterns != null && settingObject.Patterns.Length > 0)
                    cp.Patterns = settingObject.Patterns;
            }
        }

        /// <summary>
        /// Saves the plugin settings
        /// </summary>
        public void SaveSettings()
        {
            ObjectSerializer.Serialize(this.settingFilename, this.settingObject);
        }

        #endregion
    }
}