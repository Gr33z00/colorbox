﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ColorBox
{
    public class SearchResult
    {
        protected int wordStart;
        protected int wordEnd;
        protected int wordLine;
        protected string word;

        public SearchResult(int start, int end, int line, string found)
        {
            wordStart = start;
            wordEnd = end;
            wordLine = line;
            word = found;
        }

        public SearchResult() { }

        public int WordStart
        {
            get
            {
                return wordStart;
            }
            set
            {
                wordStart = value;
            }
        }

        public int WordEnd
        {
            get
            {
                return wordEnd;
            }
            set
            {
                wordEnd = value;
            }
        }

        public int WordLine
        {
            get
            {
                return wordLine;
            }
            set
            {
                wordLine = value;
            }
        }

        public string Word
        {
            get
            {
                return word;
            }
            set
            {
                word = value;
            }
        }

        public Boolean Equals(SearchResult sr)
        {
            if (sr == null)
                return false;

            if (!word.Equals(sr.Word))
                return false;

            if (WordLine != sr.WordLine)
                return false;

            if (WordStart != sr.WordStart)
                return false;

            if (WordEnd != sr.WordEnd)
                return false;

            return true;
        }
    }
}
