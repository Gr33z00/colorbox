﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using PluginCore;

namespace ColorBox.Controls
{
    public class ColorTip : Panel
    {
        protected Color currentColor;
        
        #region Control creation
		
		public ColorTip(IMainForm mainForm, Size size) : base()
		{
            base.Location = new Point(0,0);
            base.BorderStyle = BorderStyle.FixedSingle;
            base.Visible = false;
            base.Size = size;
            
            (mainForm as Form).Controls.Add(this);
		}
		
		#endregion

        #region TipMethods

        public void ShowAtLocation(Point position, Color color)
        {
            CurrentColor = color;

            base.Left = position.X;
            base.Top = position.Y;

            this.Show();
            base.BringToFront();
        }

        public Color CurrentColor
        {
            get
            {
                return currentColor;
            }
            set
            {
                currentColor = value;
                base.BackColor = currentColor;
            }
        }

        public virtual new void Show()
        {
            base.Visible = true;
            base.BringToFront();
        }
        #endregion
    }
}
