﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ScintillaNet;

namespace ColorBox
{
    public class SelectionHelper
    {

        public static bool IsMultilineSelection(ScintillaControl sci, int position)
        {
            return sci.SelectionStart != sci.SelectionEnd;
        }

        public static bool IsDeclinedStyle(ScintillaControl sci, int position)
        {
            int style = sci.BaseStyleAt(position);
            string syntax = sci.ConfigurationLanguage;

            if (style == 0)
                return false;

            if (syntax.StartsWith("as") || syntax.Equals("haxe"))
                return !(style == 4 || style == 6 || style == 12);
            else if (syntax.Equals("css"))
                return !(style == 4 || style == 8 || style == 10 || style == 11);
            else if (syntax.Equals("xml"))
                return !(style == 6 || style == 17);
            else if (syntax.Equals("html"))
                return !(style == 6);
            else
                return true;
        }

        public static SearchResult SimpleWordSearch(ScintillaNet.ScintillaControl sci, int position)
        {
            int wordStart = sci.WordStartPosition(position, true);
            int wordEnd = sci.WordEndPosition(position, true);

            /// Start workaround sci.GetWordFromPosition(position) not delivering commas

            int selStart = sci.SelectionStart;
            int selEnd = sci.SelectionEnd;

            sci.SelectionStart = wordStart;
            sci.SelectionEnd = wordEnd;
            
            String found = sci.SelText;

            sci.SelectionStart = selStart;
            sci.SelectionEnd = selEnd;

            int startLine = sci.LineFromPosition(wordStart);
            int endLine = sci.LineFromPosition(wordEnd);
            /// End workaround

            ///MessageBox.Show("Retrieved wordStart at " + wordStart + " (Position: "+position+" --> Char '"+startChar+"') (line " + startLine + ") and wordEnd at " + wordEnd + " (line " + endLine + ") --> '" + found + "'");

            return new SearchResult(wordStart, wordEnd, startLine, found);
        }

        public static SearchResult ExtendedWordSearch(ScintillaNet.ScintillaControl sci, SearchResult sr)
        {
            int endLine = sci.LineFromPosition(sr.WordEnd);
            int startLine = sr.WordLine;

            int wordStart = sr.WordStart;
            int wordEnd = sr.WordEnd;

            string found = sr.Word;

            string wordstarts = @"[>""\t\r\n=\:;{]";
            string wordends = @"[<""\t\r\n};:]";

            bool process;
            bool match;
            string c;

            if (endLine != startLine)
            {
                found = "";
                wordEnd = wordStart;
            }

            c = Char.ToString((char)sci.CharAt(wordStart));
            bool checkFirst = (c != String.Empty && !Regex.IsMatch(c,wordends) && !Regex.IsMatch(c,wordstarts));
            //MessageBox.Show("found first: " + c);
            
            c = Char.ToString((char)sci.CharAt(wordEnd-1));
            bool checkLast = (wordEnd>wordStart)?(!Regex.IsMatch(c,wordends) && !Regex.IsMatch(c,wordstarts)):true;
            //MessageBox.Show("found last: " + c);

            if (checkFirst && checkLast)
            {
                do
                {
                    process = false;
                    c = Char.ToString((char)sci.CharAt(wordEnd));
                    match = !Regex.IsMatch(c, wordends);
                    if (match)
                    {
                        found += c;
                        wordEnd++;
                        process = (sci.LineFromPosition(wordEnd) == endLine) && (wordEnd < sci.PositionFromLine(endLine)+sci.LineLength(endLine));
                    }
                }
                while (process);

                do
                {
                    process = false;
                    c = Char.ToString((char)sci.CharAt(wordStart - 1));
                    match = !Regex.IsMatch(c, wordstarts);
                    if (match)
                    {
                        found = c + found;
                        wordStart--;
                        process = (sci.LineFromPosition(wordStart) == startLine) && (wordStart >= sci.PositionFromLine(startLine));
                    }
                }
                while (process);
            }

            //Remove leading and trailing whitespaces
            int length = found.Length;
            found = found.TrimStart();
            wordStart += length - found.Length;
            length = found.Length;
            found = found.TrimEnd();
            wordEnd -= length - found.Length;
            //MessageBox.Show("Adapted wordStart at " + wordStart + " and wordEnd at " + wordEnd + " --> '" + found + "'");

            return new SearchResult(wordStart, wordEnd, startLine, found);
        }

        public static bool IsSameWord(ScintillaNet.ScintillaControl sci, int pos1, int pos2)
        {
            SearchResult sr1 = SelectionHelper.SimpleWordSearch(sci, pos1);
            return sr1.WordStart <= pos2 && sr1.WordEnd >= pos2;
        }
    }
}
